use program::Program;

mod instr;
mod program;

fn main() {
    let test = "p1^7idop33-#6e";
    println!("test program: {}", test);
    let mut program = Program::from_str(test).unwrap();
    dbg!(&program);
    if let Err(err) = program.run() {
        println!("Program failed. Reason: {:?}", err);
    }
}

// TODO:
// - Add ways to pass input to an interpreter.