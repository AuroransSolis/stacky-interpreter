use logos::Logos;

#[derive(Debug, Logos, Eq, PartialEq)]
pub enum Instr {
    #[token("i")]
    PushStdin,
    #[token("o")]
    PopPrintChar,
    #[token(".")]
    PopPrintCharEmpty,
    #[token("n")]
    PopPrintHex,
    #[token("s")]
    PopReg,
    #[token("l")]
    PushRegToStack,
    #[token("+")]
    PopSum,
    #[token("-")]
    PopSub,
    #[token("e")]
    EndProgram,
    #[token("d")]
    DuplicateStackTop,
    #[token("w")]
    SwapStackTopTwo,
    #[regex(r"p[0-9]+", |lex| lex.slice()[1..].parse())]
    PushByte(u8),
    #[regex(
        r#"p'[\w\d`~!@#$%\^&\*,.:;"/\?\{\}\[\]\\\|\(\)-_=\+]+'"#,
        |lex| {
            let slice = lex.slice();
            slice[2..slice.len() - 1].to_string()
        }
    )]
    PushString(String),
    #[regex(r"\^[0-9]+", |lex| lex.slice()[1..].parse())]
    CondJumpFwd(usize),
    #[regex(r"#[0-9]+", |lex| lex.slice()[1..].parse())]
    JumpBwd(usize),
    #[error]
    #[regex(r"[ \t\n\f]+", logos::skip)]
    Whitespace,
}
