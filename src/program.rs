use crate::instr::Instr::{self, *};
use logos::Logos;
use std::{
    fmt,
    io::{stdin, stdout, Error as IoError, ErrorKind, Result as IoResult, Write},
};

#[derive(Debug)]
pub enum RuntimeError {
    PcOutOfBounds(usize, usize),
    InsufficientStack,
    IoError(IoError),
}

impl fmt::Display for RuntimeError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            RuntimeError::PcOutOfBounds(len, pc) => {
                write!(f, "PC out of bounds: len = {}, pc = {}", len, pc)
            }
            RuntimeError::InsufficientStack => write!(f, "IM DED XP"),
            RuntimeError::IoError(err) => write!(f, "{}", err),
        }
    }
}

#[derive(Debug)]
pub struct Program {
    pub instrs: Vec<Instr>,
    pub stack: Vec<u8>,
    pub pc: usize,
    pub reg: u8,
    pub run: bool,
    pub stdin_buf: String,
}

impl Program {
    pub fn from_str(s: &str) -> IoResult<Self> {
        let mut lex = Instr::lexer(s);
        let mut instrs = Vec::new();
        while let Some(instr_res) = lex.next() {
            instrs.push(instr_res);
        }
        println!("{:?}", instrs);
        if instrs.contains(&Instr::EndProgram) {
            Ok(Program {
                instrs,
                stack: Vec::with_capacity(4096),
                pc: 0,
                reg: 0,
                run: true,
                stdin_buf: String::new(),
            })
        } else {
            Err(IoError::new(
                ErrorKind::InvalidInput,
                "Input program does not contain end program operator.",
            ))
        }
    }

    pub fn step(&mut self) -> Result<(), RuntimeError> {
        if self.run {
            match self
                .instrs
                .get(self.pc)
                .ok_or(RuntimeError::PcOutOfBounds(self.instrs.len(), self.pc))?
            {
                PushStdin => {
                    stdin()
                        .read_line(&mut self.stdin_buf)
                        .map_err(|e| RuntimeError::IoError(e))?;
                    self.stack.push(
                        self.stdin_buf
                            .trim()
                            .as_bytes()
                            .get(0)
                            .copied()
                            .unwrap_or(0),
                    );
                    self.stdin_buf.clear();
                }
                PopPrintChar => {
                    let byte = self.stack.pop().ok_or(RuntimeError::InsufficientStack)?;
                    print!("{}", byte as char);
                    stdout().flush().map_err(|err| RuntimeError::IoError(err))?;
                }
                PopPrintCharEmpty => {
                    if self.stack.is_empty() {
                        Err(RuntimeError::InsufficientStack)
                    } else {
                        while let Some(byte) = self.stack.pop() {
                            print!("{}", byte as char);
                        }
                        stdout().flush().map_err(|err| RuntimeError::IoError(err))?;
                        Ok(())
                    }?
                }
                PopPrintHex => {
                    let byte = self.stack.pop().ok_or(RuntimeError::InsufficientStack)?;
                    print!("{:x}", byte);
                    stdout().flush().map_err(|err| RuntimeError::IoError(err))?;
                }
                PopReg => {
                    let byte = self.stack.pop().ok_or(RuntimeError::InsufficientStack)?;
                    self.reg = byte;
                }
                PushRegToStack => self.stack.push(self.reg),
                PopSum => {
                    let rhs = self.stack.pop().ok_or(RuntimeError::InsufficientStack)?;
                    let lhs = self.stack.pop().ok_or(RuntimeError::InsufficientStack)?;
                    self.stack.push(lhs + rhs);
                }
                PopSub => {
                    let rhs = self.stack.pop().ok_or(RuntimeError::InsufficientStack)?;
                    let lhs = self.stack.pop().ok_or(RuntimeError::InsufficientStack)?;
                    self.stack.push(lhs - rhs);
                }
                EndProgram => self.run = false,
                DuplicateStackTop => self
                    .stack
                    .get(0)
                    .copied()
                    .map(|byte| self.stack.push(byte))
                    .ok_or(RuntimeError::InsufficientStack)?,
                SwapStackTopTwo => {
                    if self.stack.len() > 2 {
                        self.stack.swap(0, 1);
                        Ok(())
                    } else {
                        Err(RuntimeError::InsufficientStack)
                    }?
                }
                PushByte(byte) => self.stack.push(*byte),
                PushString(string) => {
                    for byte in string.bytes() {
                        self.stack.push(byte);
                    }
                }
                _ => {}
            }
            self.incr_pc()
        } else {
            Ok(())
        }
    }

    fn incr_pc(&mut self) -> Result<(), RuntimeError> {
        match self
            .instrs
            .get(self.pc)
            .ok_or(RuntimeError::PcOutOfBounds(self.instrs.len(), self.pc))?
        {
            CondJumpFwd(amt) => {
                let topval = self.stack.pop().ok_or(RuntimeError::InsufficientStack)?;
                if topval == 0 {
                    self.pc += amt;
                } else {
                    self.pc += 1;
                }
            }
            JumpBwd(amt) => self.pc -= amt,
            _ => self.pc += 1,
        }
        Ok(())
    }

    pub fn run(&mut self) -> Result<(), RuntimeError> {
        while self.run {
            self.step()?;
        }
        Ok(())
    }
}
